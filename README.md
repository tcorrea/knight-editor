# Knight Editor Demo

A point-and-click doodad editor for use with RPG Maker MV games.

This is the demo project. For usage instructions in your own project, visit:
https://forums.rpgmakerweb.com/index.php?threads/knight-editor.118752/

Note that this project requires Yanfly's Grid-Free Doodads and I cannot
redistribute a copy of it under Yanfly's terms, so the included 
'YEP_GridFreeDoodads.js' file is only an empty placeholder.

Please get a copy of the plugin from Yanfly's website and replace the included
placeholder file to run the demo. You can find it at:
http://www.yanfly.moe/wiki/Grid-Free_Doodads_(YEP)